using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCam : MonoBehaviour
{
    [Header("References")]
    public Transform orientation;
    public Transform player;
    public Transform playerObject;
    public Transform rb;

    public float rotationSpeed;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    private void Update()
    {
        //Orientar la rotaci�n
        Vector3 viewDir = player.position - new Vector3(transform.position.x, player.position.y, transform.position.z);
        orientation.forward = viewDir.normalized;

        //Rotar jugador
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 InputDir = orientation.forward * verticalInput + orientation.right * horizontalInput;

        if (InputDir != Vector3.zero)
            playerObject.forward = Vector3.Slerp(playerObject.forward, InputDir.normalized, Time.deltaTime * rotationSpeed);
    }

}
