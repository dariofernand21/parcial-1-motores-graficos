using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public GameObject timeIsUp, restartButton;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        if (TimeLeft.timeLeft <= 0)
        {
            Time.timeScale = 0;
            timeIsUp.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
        }
    }

   
    public void restartScene()
    {
        timeIsUp.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);
        Time.timeScale = 1;
        TimeLeft.timeLeft = 60f;
        SceneManager.LoadScene("Parcial");
    }
}
