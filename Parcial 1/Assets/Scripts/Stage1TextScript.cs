using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1TextScript : MonoBehaviour
{
    public GameObject uiObject;
    void Start()
    {
        uiObject.SetActive(false);
    }

    IEnumerator WaitForSeconds()
    {
        yield return new WaitForSeconds(2);
        Destroy(uiObject);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            uiObject.SetActive(true);
            StartCoroutine("WaitForSeconds");
        }



    }
}
