using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    Vector3 moveDirection;
    public Transform orientation;
    public float moveSpeed;
    public float groundDrag;
    public float boostTimer;
    bool boosting;

    //public GameOverScreen GameOverScreen;
    public GameObject VictoryText;    
    Rigidbody rb;
       
    public LayerMask whatIsGround;
    public float jumpMagnitude;
    public CapsuleCollider col;
    public float jumpForce = 5f;
    public float groundDistance = 0.5f;
    bool canDoubleJump;

    public float threshold;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        Cursor.lockState = CursorLockMode.Locked; //Definir que el mouse est oculto
        col = GetComponent<CapsuleCollider>();

        VictoryText.SetActive(false);
        moveSpeed = 5;
        boostTimer = 0;
        boosting = false;
    }
   
       void Update()
    {
        float verticalInput = Input.GetAxis("Vertical") * moveSpeed; //Definir comando de movimiento Axis Vertical (Adelante/Atras)
        float horizontalInput = Input.GetAxis("Horizontal") * moveSpeed; //Definir comando de movimiento Axis Horizontal (Izquierda/Derecha)

        verticalInput *= Time.deltaTime; //Definir que el movimiento este limitado por frame
        horizontalInput *= Time.deltaTime; //Definir que el movimiento este limitado por frame

        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;

        rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force); //Comportamiento del movimiento

        if (Input.GetKeyDown("escape")) //Comando para volver a mostrar el mouse
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if(boosting)
        {
            boostTimer += Time.deltaTime;            
            if (boostTimer >=3)
            {
                moveSpeed = 5;
                boostTimer = 0;
                boosting = false;                
            }
        }

        if (transform.position.y < threshold)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);

        }
        if (Input.GetKeyDown("r"))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        SpeedControl();
        Jumping();

    }
       

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "SpeedBoost")
        {
            boosting = true;
            moveSpeed = 9;
            Destroy(other.gameObject);
        }
        
        if (other.tag == "BoostSize")
        {
            boosting = true;
            transform.localScale += new Vector3(1, 1, 1);            
            Destroy(other.gameObject);
            VictoryText.SetActive(true);
        }

        if (other.tag == "DebuffSize")
        {
            boosting = true;
            transform.localScale -= new Vector3(1, 1, 1);
            Destroy(other.gameObject);
        }

        if (other.tag == "Obstacle")
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
                
    }      

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        // limit velocity if needed
        if (flatVel.magnitude > moveSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }

    private void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (IsGrounded())
            {
                rb.velocity = Vector3.up * jumpForce;
                canDoubleJump = true;
            }
            else if (canDoubleJump)
            {
                rb.velocity = Vector3.up * jumpForce;
                canDoubleJump = false;
            }
        }
    }

    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * 0.9f, whatIsGround);
    }

   


}

