using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncher : MonoBehaviour
{
    public GameObject Projectile;

    public Transform spawnLocation;

    public Quaternion spawnRotation;

    public float spawnTime = 2f;
     
    public float timeSinceSpawned = 0f;


    void Update()
    {
       

        timeSinceSpawned += Time.deltaTime;

        if(timeSinceSpawned >= spawnTime)
        {
            Instantiate(Projectile, spawnLocation.position, spawnRotation);

            timeSinceSpawned = 0;
            
        }
    }
}
