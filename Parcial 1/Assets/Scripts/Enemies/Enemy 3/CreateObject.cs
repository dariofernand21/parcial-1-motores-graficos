using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject Prefab;
     

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Instantiate(Prefab, spawnPoint.position, spawnPoint.rotation);
        }
        
    }

    
   
}
