using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bot : MonoBehaviour
{
    private GameObject player;
    public int speedMove;
    public float threshold;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    
    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(speedMove * Vector3.forward * Time.deltaTime);

        if (transform.position.y < threshold)
        {
            Destroy(gameObject);
        }
    }

    

}
