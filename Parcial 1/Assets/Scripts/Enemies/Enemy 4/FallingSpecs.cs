using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingSpecs : MonoBehaviour
{   
    public float timeToLive = 3f;

    public float timeSinceSpawned = 0f;       


    void Update()
    {
        
        timeSinceSpawned += Time.deltaTime;

        if (timeSinceSpawned > timeToLive)
        {
            Destroy(gameObject);
        }
    }

}
