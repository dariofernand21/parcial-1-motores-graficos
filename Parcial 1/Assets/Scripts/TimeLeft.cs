using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLeft : MonoBehaviour
{
    Text text;
    public static float timeLeft = 60f;

    void Start()
    {
        text = GetComponent<Text>();
    }
        
    void Update()
    {
        

        if (timeLeft < 0)
        {
            timeLeft = 0;           
        }

        timeLeft -= Time.deltaTime;
        text.text = "Time Left: " + timeLeft.ToString();


    }
}
