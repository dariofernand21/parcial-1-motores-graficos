using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpRotation : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(50, 50, 50) * Time.deltaTime);
    }
}
